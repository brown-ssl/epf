# EPF

We present EPF (Evil Packet Filter): a new method for bypassing
various (both deployed and proposed) kernel isolation techniques by
abusing the BPF infrastructure of the Linux kernel: i.e., by
leveraging BPF code, provided by unprivileged users/programs, as
attack payloads. We demonstrate two different EPF instances, namely
BPF-Reuse and BPF-ROP, which utilize malicious BPF payloads to mount
privilege escalation attacks in both 32- and 64-bit x86 platforms. We
also present the design, implementation, and evaluation of a set of
defenses to enforce the isolation between BPF instructions and benign
kernel data, and the integrity of BPF program execution, effectively
providing protection against EPF-based attacks. Our implemented
defenses show minimal overhead (<3%) in BPF-heavy tasks.

EPF is published at ATC '23 ([paper](https://www.usenix.org/conference/atc23/presentation/jin))

```
@inproceedings{jin2023epf,
	title		= {{EPF: Evil Packet Filter}},
	author		= {Jin, Di and Atlidakis, Vaggelis and Kemerlis, Vasileios P},
	booktitle	= {2023 USENIX Annual Technical Conference (USENIX ATC 23)},
	pages		= {735--751},
	year		= {2023}
}
```

## EPF-hardened Linux Kernel

### Building

First download Linux kernel 5.10

```
$ wget -P /tmp https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.10.tar.xz
$ tar -xJf /tmp/linux-5.10.tar.xz
$ cd linux-5.10
```

Then apply the patches to the kernel
```
$ patch -p1 < patches/0001-Add-BPF-ISR.patch
patching file include/linux/filter.h
patching file init/Kconfig
patching file kernel/bpf/core.c
$ patch -p1 < patches/0002-Apply-BPF-ISR-to-orig_prog.patch 
patching file include/linux/filter.h
patching file kernel/bpf/core.c
patching file net/core/filter.c
$ patch -p1 < patches/0003-Adding-BPF-NX.patch 
patching file arch/x86/include/asm/pgtable_64_types.h
patching file arch/x86/mm/fault.c
patching file init/Kconfig
patching file kernel/bpf/core.c
$ patch -p1 < patches/0004-Adding-BPF-CFI.patch 
patching file init/Kconfig
patching file kernel/bpf/core.c
```

Start with kernel config for kvm_guest,

```
$ make defconfig
$ make kvm_guest.config
```

disable `CONFIG_RANDOMIZE_MEMORY`, and enable `CONFIG_BPF_HARDENING`, `CONFIG_BPF_ISR`, `CONFIG_BPF_NX`, and `CONFIG_BPF_CFI` (example in menuconfig),

```
...
[*] Enable BPF interpreter hardening
[*]   Enable BPF instruction set randomization                                           
[*]     Enable bpf NX
[*]     Enable bpf CFI
...
```

then compile and install the kernel as usual.

### Config the Guest OS

Inside the guest OS, make sure that JIT is disabled

```
# sysctl net.core.bpf_jit_enable=0
```

Or disable it as default

```
# sysctl -w net.core.bpf_jit_enable=0 >> /etc/sysctl.conf
# sysctl -p /etc/sysctl.conf
```

## Benchmarks and Exploits

Exploits done in the paper are available under `exploits` directory. And benchmarks are under `benchmark_network` and `benchmark_seccomp`
