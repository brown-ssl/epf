#!/usr/bin/python

import sys
import math

s = sys.stdin.read()
nums = [float(k) for k in s.split()]
mean = sum(nums)/len(nums)
sqloss = [(k-mean)*(k-mean) for k in nums]
vars = sum(sqloss)/len(sqloss)
stddev = math.sqrt(vars)
print("{},{}".format(mean, stddev))
