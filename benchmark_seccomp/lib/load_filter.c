#define _GNU_SOURCE 1
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sched.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <sys/prctl.h>
#include <errno.h>
#include <linux/seccomp.h>
#include <linux/filter.h>
#include <sys/ptrace.h>

#define HEADER_LEN 5
/* allow program to print and exit gracefully regardless of tested policy */
static struct sock_filter *add_header(struct sock_filter *filter)
{
	struct sock_filter header[] = {
		BPF_STMT(BPF_LD | BPF_W | BPF_ABS,
                        (offsetof(struct seccomp_data, nr))),
		BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, 96, 2, 0), /* gettimeofday */
		BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, 1, 1, 0), /* write */
		BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, 231, 0, 1), /* exit_group */
		BPF_STMT(BPF_RET | BPF_K, SECCOMP_RET_ALLOW),
	};
	memcpy(filter, header, HEADER_LEN*sizeof(header[0]));
	return &filter[HEADER_LEN];
};

static void force_ret_errno(struct sock_filter *filter, int size)
{
	int i;
	for (i = 0; i < size; i++) {
		if ((filter[i].code == (BPF_RET | BPF_K)) || (filter[i].code == (BPF_RET | BPF_X))) {
			filter[i].code = BPF_RET | BPF_K;
			filter[i].k = SECCOMP_RET_ERRNO | (1 & SECCOMP_RET_DATA);
		}
	}
};

static int install_syscall_filter(int filter_fd)
{
	struct sock_filter *filter, *body;
	int count = 0;
	filter = (struct sock_filter*) malloc(sizeof(struct sock_filter) * 2048);
	memset(filter, 0, sizeof(*filter)*2048);
	body = add_header(filter);
	count += read(filter_fd, body, 0x100000-HEADER_LEN);
	if (count == 0) {
		count = 8;
		body->code = BPF_RET | BPF_K;
		body->k = SECCOMP_RET_KILL_PROCESS;
	};
	force_ret_errno(body, count/sizeof(filter[0]));
	struct sock_fprog prog = {
		.len = (unsigned short)(count/sizeof(filter[0]) + HEADER_LEN),
		.filter = filter,
	};
	/* printf("filter = %p, len = %d\n", filter, prog.len); */

	if (prctl(PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0)) {
		perror("prctl(NO_NEW_PRIVS)");
		goto failed;
	}
	if (syscall(SYS_seccomp, SECCOMP_SET_MODE_FILTER, 0 /* SECCOMP_FILTER_FLAG_SPEC_ALLOW */, &prog)) {
		perror("prctl(SECCOMP)");
		goto failed;
	}
	return 0;

failed:
	if (errno == EINVAL)
		fprintf(stderr, "SECCOMP_FILTER is not available. :(\n");
	return 1;
}

int main(int argc, char *argv[])
{
	/* char buf[1024]; */
	pid_t pid;
	struct timeval start, end, dif;
        cpu_set_t set;
	int i, j, filter_fd;
	if (argc < 2) return 1;
	filter_fd = open(argv[1], O_RDONLY);
	if (filter_fd < 0) return 1;
        CPU_ZERO(&set);
        CPU_SET(1, &set);
        if (sched_setaffinity(getpid(), sizeof(set), &set) == -1)
            perror("setaffinity");
	if (install_syscall_filter(filter_fd))
            perror("can't install filter");
        for (i = 0; i < 30; i++) {
            gettimeofday(&start, NULL);
            for (j = 0; j < 10000; j++) {
		pid = syscall(i*4+21, 0, 0, 0, 0);
            }
            gettimeofday(&end, NULL);
            timersub(&end, &start, &dif);
            printf("%ld\n", dif.tv_usec);
        }
	return 0;
}
