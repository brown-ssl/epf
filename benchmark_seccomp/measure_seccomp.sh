#!/bin/bash
set -euo pipefail

FILTER_DIR=$(dirname $0)/filters_x64
SCRIPT_DIR=$(dirname $0)/lib
OUTPUT_DIR=$(dirname $0)/outputs

mkdir -p $FILTER_DIR
mkdir -p $SCRIPT_DIR
mkdir -p $OUTPUT_DIR

output_name=$1
ofile=$OUTPUT_DIR/$output_name
filter_files=$(find ${FILTER_DIR} -name '*_filter' | sort)
rm -rf $ofile
touch $ofile
for f in $filter_files; do
    printf "%s," $(basename $f) >> $ofile
    $SCRIPT_DIR/load_filter $f | $SCRIPT_DIR/m_v.py >> $ofile
done
