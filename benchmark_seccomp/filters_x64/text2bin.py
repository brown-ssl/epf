#!/usr/bin/python3
import sys

if len(sys.argv) < 3:
    print('Usage: ./text2bin.py [filename] [outputfile]')

with open(sys.argv[1]) as fin, open(sys.argv[2], 'wb') as fout:
    ks = fin.read().split()
    for k in ks:
        fout.write(int(k, 16).to_bytes(4, byteorder='little'))
