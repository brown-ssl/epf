#define _GNU_SOURCE 1
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sched.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <errno.h>
#include <linux/unistd.h>
#include <linux/audit.h>
#include <linux/filter.h>
#include <linux/seccomp.h>
#include <sys/prctl.h>

static int install_syscall_filter(int filter_fd)
{
	struct sock_filter *filter;
	int count;
	filter = (struct sock_filter*) malloc(sizeof(struct sock_filter) * 2048);
	memset(filter, 0, sizeof(*filter)*2048);
	count = read(filter_fd, filter, 0x100000);
	struct sock_fprog prog = {
		.len = (unsigned short)(count/sizeof(filter[0])),
		.filter = filter,
	};

	if (prctl(PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0)) {
		perror("prctl(NO_NEW_PRIVS)");
		goto failed;
	}
	if (prctl(PR_SET_SECCOMP, SECCOMP_MODE_FILTER, &prog)) {
		perror("prctl(SECCOMP)");
		goto failed;
	}
	return 0;

failed:
	if (errno == EINVAL)
		fprintf(stderr, "SECCOMP_FILTER is not available. :(\n");
	return 1;
}

int main(int argc, char *argv[])
{
	/* char buf[1024]; */
	pid_t pid;
	struct timeval start, end, dif;
        cpu_set_t set;
	int i, j, filter_fd;
	if (argc < 2) return 1;
	filter_fd = open(argv[1], O_RDONLY);
	if (filter_fd < 0) return 1;
        CPU_ZERO(&set);
        CPU_SET(1, &set);
        if (sched_setaffinity(getpid(), sizeof(set), &set) == -1)
            perror("setaffinity");
	if (install_syscall_filter(filter_fd))
            perror("can't install filter");
        for (i = 0; i < 100; i++) {
            gettimeofday(&start, NULL);
            for (j = 0; j < 100000; j++) {
		pid = syscall(SYS_getpid, 0, 0, 0, 0);
            }
            gettimeofday(&end,NULL);
            timersub(&end, &start, &dif);
            printf("%ld\n", dif.tv_usec);
        }
	return 0;
}
