#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <sys/sysinfo.h>

#include "testcases.h"

#define MAX_CHILD 100000

struct spray_worker {
    long size;
    int pid;
    int fd;
    int skip;
    int dead;
};

struct spray_worker workers[MAX_CHILD];
int max_child = 0;
long total_mem = 0;

struct spray_setup {
    void (*spray_func)(int, int);
    int timeout;
    const char *name;
};

#define SETUP_NUM 15
struct spray_setup setups[] = {
    {test_seccomp, 5000, "seccomp"},
    {test_dummy, 100000, "dummy"},
    {test_readv, 500, "readv"},
    {test_pipe, 500000, "pipe"},
    {test_sem, 5000, "semget"},
    {test_msg, 5000000, "msgget"},
    {test_bpf, 5000000, "bpf"},
    {test_tkey, 100000, "add_key"},
    {test_tsso, 500000, "setsockopt"},
};

int mode = 1;
    
#define MAX_NAME 1024
const char *oom_score_file = "/proc/%d/oom_score_adj";

int change_oom_score(int score)
{
	int fd;
	char filename[MAX_NAME];
	snprintf(filename, MAX_NAME, oom_score_file, getpid());
	fd = open(filename, O_WRONLY, 0);
	if (fd < 0) return -1;
	dprintf(fd, "%d", score);
	close(fd);
	return 0;
}

long poll_results(void)
{
    int i, count, nr_reset = 3, reset_round = 0;
    long value = 0, cur_mem = 0;
retry:
    for (i = 0; i < max_child; i++) {
	int commfd = workers[i].fd;
	if (workers[i].skip)
	    continue;
	while ((count = read(commfd, &value, sizeof(value))) > 0) {
	    if (value == -1) {
		workers[i].skip = 1;
		break;
	    }
	    workers[i].size += value;
	    cur_mem += value;
	};
	if (count == 0 ||
	    count == -1 && (errno == EAGAIN || errno == EWOULDBLOCK)) {
	    reset_round = 1;
	};
    };
    if (reset_round && nr_reset) {
	nr_reset--;
	reset_round = 0;
	usleep(setups[mode].timeout);
	goto retry;
    };
    printf("cur_mem = %ld\n", cur_mem);
    return cur_mem;
}

void wait_start(int i)
{
    int flags;
    long v;
    int commfd = workers[i].fd;
    read(commfd, &v, sizeof(v));
    flags = fcntl(commfd, F_GETFL);
    if (fcntl(commfd, F_SETFL, flags | O_NONBLOCK)) {
	perror("fcntl");
	exit(1);
    }
}

int is_parent = 1;

int fork_child(int i)
{
    int pipefd[2];
    int pid;
    if (pipe(pipefd)) {
	perror("pipe");
	exit(1);
    }
    pid = fork();
    if (pid < 0) {
	perror("fork");
	return -1;
    }

    if (pid == 0) {
	is_parent = 0;
	change_oom_score(10);
	close(pipefd[0]);
	printf("mode = %d\n", mode);
	setups[mode].spray_func(pipefd[1], i);
	close(pipefd[1]);
	return 0;
    } else {
	close(pipefd[1]);
	workers[i].size = 0;
	workers[i].pid = pid;
	workers[i].fd = pipefd[0];
	workers[i].skip = 0;
	workers[i].dead = 0;
	wait_start(i);
    }
    return 0;
}

long dead_memory(void)
{
    int i;
    long sum = 0;
    for (i = 0; i < max_child; i++) {
        if (workers[i].dead == 1) {
            sum += workers[i].size;
	}
    }
    return sum;
}

void stop_and_exit(int ignore)
{
    if (is_parent)
        printf("total_mem = %ld, dead = %ld\n", total_mem, dead_memory());
    exit(0);
}

void mark_dead(int ignore)
{
    int pid, i;
    pid = wait(NULL);
    printf("someone is dead\n");
    for (i = 0; i < max_child; i++) {
	if (workers[i].pid == pid) {
	    workers[i].dead = 1;
	}
    }
}

int check_system_state(void)
{
    int i;
    struct sysinfo info;
    if (sysinfo(&info) < 0) {
	perror("sysinfo");
	exit(1);
    }
    printf("freemem is %ld\n", info.freeram * info.mem_unit);
    if (info.freeram * info.mem_unit < 1000 * 1000 * 10) {
	printf("approaching oom, stopping\n");
	return -1;
    }
    for (i = 0; i < max_child; i++) {
	if (workers[i].dead)
	    return -1;
    }
    return 0;
}

int main(int argc, char *argv[])
{
    int i, ret;
    char a;
    long cur_mem = 0;
    if (argc > 1) {
	mode = atoi(argv[1]);
	printf("testing %s\n", setups[mode].name);
    }
    signal(SIGINT, stop_and_exit);
    signal(SIGCHLD, mark_dead);
    for (i = 0; i < MAX_CHILD; i++) {
	ret = fork_child(i);
	if (ret < 0)
	    break;
	max_child++;
	cur_mem = poll_results();
	if (check_system_state())
	    break;
	if (cur_mem == 0)
	    break;
	else
	    total_mem += cur_mem;
    }
    printf("total_mem = %lu, with %d children\n", total_mem - dead_memory(), max_child);
    scanf("%c", &cur_mem);
    for (i = 0; i < max_child; i++) {
	kill(workers[i].pid, SIGKILL);
	waitpid(workers[i].pid, NULL, WNOHANG);
    }
    return 0;
}
