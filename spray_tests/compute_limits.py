import subprocess
import shlex
import resource
import os

SIZE_IOVEC = 16
PID_FILE = '/sys/fs/cgroup/user.slice/user-{}.slice/pids.max'.format(os.getuid())
PIPE_HARD_FILE = '/proc/sys/fs/pipe-user-pages-hard'
PAGE_SIZE = 4096
TCP_MEM_FILE = '/proc/sys/net/ipv4/tcp_mem'
UDP_MEM_FILE = '/proc/sys/net/ipv4/udp_mem'
CBPF_LIMIT = 3 * PAGE_SIZE
SEM_LIMIT_FILE = '/proc/sys/kernel/sem'
SEM_SIZE = 4
MSG_MNI_FILE = '/proc/sys/kernel/msgmni'
MSG_MNB_FILE = '/proc/sys/kernel/msgmnb'
SECCOMP_LIMIT_PROC = (1<<18)
AIO_LIMIT_FILE = '/proc/sys/fs/aio-max-nr'
KIOCTX_SIZE = 576
KEY_LIMIT_FILE = '/proc/sys/kernel/keys/maxbytes'

def int_command(command):
    result = subprocess.run(shlex.split(command), stdout=subprocess.PIPE)
    return result.stdout.decode('UTF-8')

def read_file(filename):
    with open(filename) as f:
        return f.read()

def hard_limit(r):
    return resource.getrlimit(r)[1]

def process_limit():
    return min(int(hard_limit(resource.RLIMIT_NPROC)), int(read_file(PID_FILE)))

def file_limit():
    return hard_limit(resource.RLIMIT_NOFILE)

def readv_limit():
    return 1024 * SIZE_IOVEC * process_limit()

def pipe_limit():
    pipe_hard = int(read_file(PIPE_HARD_FILE)) 
    if pipe_hard == 0 or pipe_hard > file_limit():
        return file_limit() * PAGE_SIZE
    else:
        return pipe_hard * PAGE_SIZE

def select_limit():
    return process_limit() * 1024 * 24

def socket_tcp_limit():
    tcp_mems = read_file(TCP_MEM_FILE)
    hard_page_limit = int(tcp_mems.split()[2])
    return hard_page_limit * PAGE_SIZE

def socket_udp_limit():
    udp_mems = read_file(UDP_MEM_FILE)
    pressure_page_limit = int(udp_mems.split()[0])
    return pressure_page_limit * PAGE_SIZE

def setsockopt_limit():
    return CBPF_LIMIT * file_limit()

def semget_limit():
    semni, sem_total, _, semns = [int(n) for n in read_file(SEM_LIMIT_FILE).split()]
    return SEM_SIZE * min(semni * semns, sem_total)

def msgsnd_limit():
    msgmni = int(read_file(MSG_MNI_FILE))
    msgmnb = int(read_file(MSG_MNB_FILE))
    return msgmni * msgmnb

def seccomp_limit():
    return SECCOMP_LIMIT_PROC * process_limit()

def aio_limit():
    aio_limit = int(read_file(AIO_LIMIT_FILE ))
    return aio_limit * KIOCTX_SIZE

def mq_limit():
    return hard_limit(resource.RLIMIT_MSGQUEUE)

def bpf_limit():
    return hard_limit(resource.RLIMIT_MEMLOCK)

def key_limit():
    return int(read_file(KEY_LIMIT_FILE))

#print(int_command('cat /proc/sys/net/ipv4/udp_mem'))
print('readv: {}'.format(readv_limit()))
print('pipe: {}'.format(pipe_limit()))
print('select: {}'.format(select_limit()))
print('tcp: {}'.format(socket_tcp_limit()))
print('udp: {}'.format(socket_udp_limit()))
print('setsockopt: {}'.format(setsockopt_limit()))
print('semget: {}'.format(semget_limit()))
print('msgget: {}'.format(msgsnd_limit())) 
print('seccomp: {}'.format(seccomp_limit()))
print('aio: {}'.format(aio_limit()))
print('bpf: {}'.format(bpf_limit()))
print('mq: {}'.format(mq_limit()))
print('keyrings: {}'.format(key_limit()))
