#include <stdio.h>
#include <sys/xattr.h>
#include <sys/types.h>

int main(int argc, char *argv[]) {
    if (setxattr("afile", "user.usage", "none", 5, 0) < 0) {
	 perror("setattr");
    } 
    return 0;
}
