#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <assert.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <linux/bpf.h>

#define LOG_BUF_SIZE 512

/* char bpf_log_buf[LOG_BUF_SIZE]; */

int
bpf_create_map(enum bpf_map_type map_type,
	       unsigned int key_size,
	       unsigned int value_size,
	       unsigned int max_entries)
{
  union bpf_attr attr = {
			 .map_type    = map_type,
			 .key_size    = key_size,
			 .value_size  = value_size,
			 .max_entries = max_entries
  };
  
  return syscall(__NR_bpf, BPF_MAP_CREATE, &attr, sizeof(attr));
}

void test_bpf_map(int commfd) {
    int i, ret;
    long value;
    /* printf("%x\n", insns[0].code); */
    for (i = 0; i < 10; i++) {
	ret = bpf_create_map(BPF_MAP_TYPE_ARRAY, 4, 4, 1<<20);
	if (ret < 0) {
	    perror("bpf");
	    value = -1;
	    write(commfd, &value, sizeof(value));
	    break;
	} else {
	    value = 8 * (1<<20);
	    write(commfd, &value, sizeof(value));
	    printf("%d done\n", i);
	}
    }
    while (1) {
	sleep(10);
    }
}
