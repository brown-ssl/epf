#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <assert.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <linux/bpf.h>

#define LOG_BUF_SIZE 512

char bpf_log_buf[LOG_BUF_SIZE];

int bpf_prog_load(enum bpf_prog_type type,
		  const struct bpf_insn *insns, int insn_cnt,
		  const char *license)
{
    union bpf_attr attr = {
			   .prog_type = type,
			   .insns     = (uint64_t) insns,
			   .insn_cnt  = insn_cnt,
			   .license   = (uint64_t) license,
			   .log_buf   = (uint64_t) bpf_log_buf,
			   .log_size  = LOG_BUF_SIZE,
			   .log_level = 1,
    };
    
    return syscall(__NR_bpf, BPF_PROG_LOAD, &attr, sizeof(attr));
}

void test_bpf(int commfd, int ignore) {
    int i, ret;
    long value;
    struct bpf_insn insns[] = {
        {BPF_LD | BPF_IMM | BPF_DW, 0x0, 0x0, 0x0, 0xdeadbeef},
        {BPF_LD | BPF_IMM, 0x0, 0x0, 0x0, 0x0},
	{BPF_JMP | BPF_EXIT, 0x0, 0x0, 0x0, 0x0},
    };
    /* printf("%x\n", insns[0].code); */
    for (i = 0; i < 100000; i++) {
	ret = bpf_prog_load(BPF_PROG_TYPE_SOCKET_FILTER, insns, sizeof(insns)/sizeof(insns[0]), "GPL");
	if (ret < 0) {
	    printf("%s\n", bpf_log_buf);
	    perror("bpf");
	    value = -1;
	    write(commfd, &value, sizeof(value));
	    break;
	} else {
	    value = 4096;
	    write(commfd, &value, sizeof(value));
	    printf("%d done\n", i);
	}
    }
    while (1) {
	sleep(10);
    }
}
