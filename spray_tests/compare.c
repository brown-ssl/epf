#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pthread.h>

#define MAX_CHILD 32700

int all_exit = 0;

void *workload(void *ignore)
{
    char *buf = malloc(128);
    while (all_exit == 0) {
	sleep(1);
    }
    return NULL;
}

void test_thread(void)
{
    int ret, i;
    pthread_t threads[MAX_CHILD];
    for (i = 0; i < MAX_CHILD; i++) {
	if (i % 1000 == 999)
	    printf("doing %d\n", i);
	ret = pthread_create(&threads[i], NULL, workload, NULL);
	if (ret != 0) {
	    printf("do %d\n", i);
	    perror("fork");
	    goto fail_out;
	}
    }
fail_out:
    ret = getchar();
    all_exit = 1;
    for (i--; i >= 0; i--) {
	pthread_join(threads[i], NULL);
    }
}

void test_fork(void)
{
    int i, pid[MAX_CHILD];
    for (i = 0; i < MAX_CHILD; i++) {
	if (i % 1000 == 999)
	    printf("doing %d\n", i);
	pid[i] = fork();
	if (pid[i] < 0) {
	    printf("do %d\n", i);
	    perror("fork");
	    goto fail_out;
	} else if (pid[i] == 0) {
	    workload(NULL);
	    /* shouldn't return */
	}
    }
    printf("done\n");
fail_out:
    i = getc(stdin);
    for (i = 0; i < MAX_CHILD; i++) {
	if (pid[i] > 0)
	    kill(pid[i], SIGKILL);
    }
    wait(NULL);
}

int main(void)
{
    test_fork();
}
