#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>

#define N 1
#define SIZE 32000
void test_sem(int commfd, int j)
{
    int i;
    long mem = 0;
    write(commfd, &mem, sizeof(mem));
    for (i = 0; i < N; i++) {
	mem = 64 * SIZE;
	if (semget(IPC_PRIVATE, SIZE, 0) < 0) {
	    perror("semget");
	    mem = -1;
	    write(commfd, &mem, sizeof(mem));
	}
	write(commfd, &mem, sizeof(mem));
    }
}
