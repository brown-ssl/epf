#include <stdio.h>
#include <stdlib.h>
#include <sys/select.h>
#include <sys/syscall.h>
#include <unistd.h>

#define N 65536
#define n (N/8)
char buf[n+1];

void test_select(int commfd, int ignore)
{
    int i;
    long mem = 0;
    fd_set *fdbuf = (fd_set *)buf;
    for (i = 0; i < n; i++) {
	buf[i] = 0xda;
    }
    for (i = 0; i < N; i++) {
	int fd = dup(0);
	if (fd < 0) {
	    perror("dup");
	    exit(1);
	}
    }
    write(commfd, &mem, sizeof(mem));
    mem = n * 2;
    write(commfd, &mem, sizeof(mem));
    if (syscall(__NR_select, N, fdbuf, NULL, fdbuf, NULL) < 0) {
	perror("select");
    };
    mem = -mem;
    write(commfd, &mem, sizeof(mem));
    printf("select wakes up\n");
    while (1) {
	sleep(10);
    }
}
