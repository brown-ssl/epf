#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <linux/keyctl.h>

#define BUF_SIZE 1024
#define N 1000

int test_tkey(int commfd, int j) {
    char buf[BUF_SIZE];
    char name[50];
    long value = 0;
    int ret, i;
    write(commfd, &value, sizeof(value));
    
    memset(buf, '\xca', BUF_SIZE);
    for (i = 0; i < N; i++) {
	sprintf(name, "ukey:%d:%d", i, j);
	ret = syscall(__NR_add_key, "user", name, buf, BUF_SIZE, KEY_SPEC_PROCESS_KEYRING);
	if (ret < 0) {
	    goto exit;
	} else {
	    value = BUF_SIZE;
	    write(commfd, &value, sizeof(value));
	}
    }
exit:
    value = -1;
    write(commfd, &value, sizeof(value));
    while (1) {
	usleep(1000000);
    }
    return 0;
}
