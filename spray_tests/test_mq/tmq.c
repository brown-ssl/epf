#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>

#define QN 9
#define NUM 10
#define SIZE 8192
int test_mq(int commfd, int nr_queue) {
    int mq[QN], i, j;
    char msg[SIZE];
    char rcv[SIZE];
    char filename[128];
    char *prefix = "";
    struct mq_attr attr = {
	.mq_flags = 0,
	.mq_maxmsg = NUM,
	.mq_msgsize = 8192,
	.mq_curmsgs = 0,
    };
    
    if (argc > 2) {
	prefix = argv[1];
    };
    
    memset(msg, 'X', SIZE);
    memset(rcv, 0, SIZE);
    for (j = 0; j < QN; j++) {
	sprintf(filename, "/randomqueue%s%d", prefix, j);
	printf("%s\n", filename);
	mq[j] = mq_open(filename, O_RDWR|O_CREAT, 0644, &attr);
	if (mq[j] < 0) {
	    perror("mq");
	    continue;
	}
	for (i = 0; i < NUM; i++) {
	    if (mq_send(mq[j], msg, SIZE, 0) < 0) perror("mq_send");
	}
	printf("qnum %3d\n", j);
	fflush(stdout);
    }
    for (j = 0; j < QN; j++) {
	if (mq[j] < 0) continue;
	for (i = 0; i < NUM; i++) {
	    if (mq_receive(mq[j], rcv, SIZE, 0) < 0) perror("mq_receive");
	}
    }
    for (j = 0; j <= QN; j++) {
	sprintf(filename, "/randomqueue%s%d", prefix, j);
	if (mq_unlink(filename) < 0 && errno != ENOENT) perror("mq_unlink");
    }

    printf("rcv[0] = %c\n", rcv[0]);
    return 0;
}
