#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>

#define QN 1
#define NUM 10
#define SIZE 8192
int main(int argc, char *argv[]) {
    int mq[QN], i, j=0;
    char msg[SIZE];
    char rcv[SIZE];
    char *filename = "/randomqueue0";
    
    if (mq_unlink(filename) < 0 && errno != ENOENT) perror("mq_unlink");
    memset(msg, 'X', SIZE);
    memset(rcv, 0, SIZE);
    printf("%s\n", filename);
    mq[j] = mq_open(filename, O_RDWR|O_CREAT);
    if (mq[j] < 0) {
	perror("mq");
    }
    for (i = 0; i < NUM; i++) {
	if (mq_send(mq[j], msg, SIZE, 0) < 0) perror("mq_send");
    }
    printf("qnum %3d\n", j);
    fflush(stdout);
    
    for (i = 0; i < NUM; i++) {
	if (mq_receive(mq[j], rcv, SIZE, 0) < 0) perror("mq_receive");
    }
    if (mq_unlink(filename) < 0 && errno != ENOENT) perror("mq_unlink");

    printf("rcv[0] = %c\n", rcv[0]);
    return 0;
}
