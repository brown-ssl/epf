#define _GNU_SOURCE
#include <errno.h>
#include <fcntl.h>
#include <linux/landlock.h>
#include <linux/prctl.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/prctl.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <unistd.h>

#ifndef landlock_create_ruleset
static inline int landlock_create_ruleset(
		const struct landlock_ruleset_attr *const attr,
		const size_t size, const __u32 flags)
{
	return syscall(__NR_landlock_create_ruleset, attr, size, flags);
}
#endif

#ifndef landlock_add_rule
static inline int landlock_add_rule(const int ruleset_fd,
		const enum landlock_rule_type rule_type,
		const void *const rule_attr, const __u32 flags)
{
	return syscall(__NR_landlock_add_rule, ruleset_fd, rule_type,
			rule_attr, flags);
}
#endif

#ifndef landlock_restrict_self
static inline int landlock_restrict_self(const int ruleset_fd,
		const __u32 flags)
{
	return syscall(__NR_landlock_restrict_self, ruleset_fd, flags);
}
#endif

int main(const int argc, char *const argv[], char *const *const envp)
{
	int ruleset_fd;
	int i, j;
	struct landlock_path_beneath_attr path_beneath = {
		.parent_fd = -1,
	};
	struct landlock_ruleset_attr ruleset_attr = {
                .handled_access_fs = 0x1fff,
	};

	if (prctl(PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0)) {
		perror("Failed to restrict privileges");
		exit(1);
	}
	for (i = 0; i < 64; i++) {
		ruleset_fd = landlock_create_ruleset(&ruleset_attr, sizeof(ruleset_attr), 0);
		if (ruleset_fd < 0) {
			const int err = errno;

			perror("Failed to create a ruleset");
			switch (err) {
			case ENOSYS:
				fprintf(stderr, "Hint: Landlock is not supported by the current kernel. "
					"To support it, build the kernel with "
					"CONFIG_SECURITY_LANDLOCK=y and prepend "
					"\"landlock,\" to the content of CONFIG_LSM.\n");
				break;
			case EOPNOTSUPP:
				fprintf(stderr, "Hint: Landlock is currently disabled. "
					"It can be enabled in the kernel configuration by "
					"prepending \"landlock,\" to the content of CONFIG_LSM, "
					"or at boot time by setting the same content to the "
					"\"lsm\" kernel parameter.\n");
			break;
			}
			return 1;
		}
		for (j = 0; j < 65536; j++) {
			char fname[1024];
			memset(fname, 0, 1024);
			snprintf(fname, 1024, "hello%d", j);
			path_beneath.parent_fd = open(fname, O_PATH |
						      O_CLOEXEC);
			if (path_beneath.parent_fd < 0) {
				perror("open");
				exit(1);
			}
			path_beneath.allowed_access = 0x1234;
			if (landlock_add_rule(ruleset_fd, LANDLOCK_RULE_PATH_BENEATH,
					      &path_beneath, 0)) {
				fprintf(stderr, "Failed to update the ruleset with \"%s\": %s\n",
					fname, strerror(errno));
				close(path_beneath.parent_fd);
				exit(1);
			}
			close(path_beneath.parent_fd);
		}
		if (landlock_restrict_self(ruleset_fd, 0)) {
			printf("i = %d\n", i);
			perror("Failed to enforce ruleset");
			exit(1);
		}
		close(ruleset_fd);
	}
	printf("done\n");
	while (1) {
		sleep(1);
	}
}
