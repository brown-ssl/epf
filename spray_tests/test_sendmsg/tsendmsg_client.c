#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <errno.h>

#define TEST_BUF 10000
#define SEND_BUF 5000000

char *message = "Hi! I'm here\n";
char *str0, *str1;

int do_it(int sockfd) {
    int i;
    struct cmsghdr *cmsg;
    struct msghdr msg;
    struct iovec iov[1];
    cmsg = (struct cmsghdr*)str0;
    msg.msg_name = NULL;
    msg.msg_namelen = 0;
    msg.msg_iov = iov;
    msg.msg_iovlen = 1;
    msg.msg_control = str0;
    msg.msg_controllen = TEST_BUF;
    iov[0].iov_base = str1;
    iov[0].iov_len = SEND_BUF;
    for (i = 0; i < TEST_BUF; i++) {
	str0[i] = 0xaa;
	str1[i] = 0xbb;
    }
    cmsg->cmsg_len = TEST_BUF;
    cmsg->cmsg_level = 100;
    cmsg->cmsg_type = 0;
    strncpy(str1, message, 200);
    printf("[-] sending...");
    if (sendmsg(sockfd, &msg, 0) < 0) {
	printf("%s\n", strerror(errno));
    } else {
	printf("Success!\n");
    }
}

#define print_and_exit(str) do {perror(str); return;} while (0)

void do_client() {
    int sockfd;
    struct sockaddr_in addr;
    char *buf;

    buf = malloc(SEND_BUF);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) print_and_exit("[x] socket failed");
    printf("[-] socket success!\n");
    
    
    /* connecting */
    memset(&addr, 0, sizeof(struct sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    addr.sin_port = htons(8008);
    if (connect(sockfd, (struct sockaddr*)&addr, sizeof(struct sockaddr_in)) < 0)
	print_and_exit("[x] connect failed");
    printf("[-] connect success!\n");

    /* sending */
    strncpy(buf, message, 128);
    if (send(sockfd, buf, SEND_BUF, 0) < 0) print_and_exit("[x] error in send");
    printf("[-] send success!\n");

    /* do_it(sockfd); */
    
    /* exiting */
    close(sockfd);
}

#define NUM 1000
int main(int argc, char *argv[]) {
    do_client();
    return 0;
}
