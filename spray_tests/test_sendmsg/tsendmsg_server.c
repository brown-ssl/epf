#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <errno.h>

#define TEST_BUF 10000
#define SEND_BUF 5000000

char *message = "Hi! I'm here\n";
char *str0, *str1;

#define print_and_exit(str) do {perror(str); return;} while (0)

void do_server() {
    int sockfd, connfd, size;
    socklen_t in_addr_size;
    struct sockaddr_in addr, in_addr;
    char buf[128];
    memset(&addr, 0, sizeof(struct sockaddr_in));

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) print_and_exit("[x] socket failed");
    printf("[-] socket success!\n");

    /* binding */
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(8008);
    if (bind(sockfd, (struct sockaddr*)&addr, sizeof(struct sockaddr_in)) < 0)
	print_and_exit("[x] bind failed");
    printf("[-] bind success!\n");

    /* listening */
    if (listen(sockfd, 100000) < 0)
	print_and_exit("[x] listen failed");
    printf("[-] start listening\n");

    getc(stdin);
    /* accepting */
    in_addr_size = sizeof(struct sockaddr_in);
    connfd = accept(sockfd, (struct sockaddr*)&in_addr, &in_addr_size);
    if (connfd < 0) print_and_exit("[x] accept failed");
    printf("[-] accept!\n");

    /* receiving */
    while ((size = recv(connfd, buf, 128, 0)) > 0) {
	write(1, buf, size);
    }

    /* exiting */
    if (size == 0) {
	printf("[-] Exiting!\n");
    } else {
	print_and_exit("[x] error in recv");
    }
    close(connfd);
    close(sockfd);
}

int main(int argc, char *argv[]) {
    do_server();
    return 0;
}
