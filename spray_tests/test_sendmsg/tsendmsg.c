#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <errno.h>
#include <sys/syscall.h>


#define TEST_BUF 10000
#define SEND_BUF 5000000

char *message = "Hi! I'm here\n";
char *str0, *str1;

int do_it(int sockfd) {
    int i;
    struct cmsghdr *cmsg;
    struct msghdr msg;
    struct iovec iov[1];
    cmsg = str0;
    msg.msg_name = NULL;
    msg.msg_namelen = 0;
    msg.msg_iov = iov;
    msg.msg_iovlen = 1;
    msg.msg_control = str0;
    msg.msg_controllen = TEST_BUF;
    iov[0].iov_base = str1;
    iov[0].iov_len = SEND_BUF;
    for (i = 0; i < TEST_BUF; i++) {
	str0[i] = 0xaa;
	str1[i] = 0xbb;
    }
    cmsg->cmsg_len = TEST_BUF;
    cmsg->cmsg_level = 100;
    cmsg->cmsg_type = 0;
    strncpy(str1, message, 200);
    printf("START\n");
    fflush(NULL);
    if (sendmsg(sockfd, &msg, 0) < 0) {
	printf("ERROR\n");
    } else {
	printf("DONE\n");
    }
    fflush(NULL);
}

#define print_and_exit(str) do {perror(str); return;} while (0)

void do_client(int port) {
    int sockfd, logfd;
    struct sockaddr_in addr;
    char *buf;
    char filename[128];
    pid_t pid;
    pid = getpid();
    sprintf(filename, "log/log%d", pid);
    logfd = open(filename, O_CREAT|O_WRONLY|O_TRUNC, 0644);
    dup2(logfd, 1);
    dup2(logfd, 2);
    buf = malloc(SEND_BUF);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) print_and_exit("[x] socket failed");
    printf("[-] socket success!\n");
    fflush(NULL);
    
    /* connecting */
    memset(&addr, 0, sizeof(struct sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    addr.sin_port = htons(port);
    if (connect(sockfd, (struct sockaddr*)&addr, sizeof(struct sockaddr_in)) < 0)
	print_and_exit("[x] connect failed");
    printf("[-] connect success!\n");
    fflush(NULL);
    
    /* sending */
    /* strncpy(buf, message, 128); */
    /* if (send(sockfd, buf, SEND_BUF, 0) < 0) print_and_exit("[x] error in send"); */
    /* printf("[-] send success!\n"); */

    do_it(sockfd);
    
    /* exiting */
    close(sockfd);
    close(logfd);
}

void do_server(int port) {
    int sockfd, connfd, size;
    socklen_t in_addr_size;
    struct sockaddr_in addr, in_addr;
    char buf[128];
    memset(&addr, 0, sizeof(struct sockaddr_in));

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) print_and_exit("[x] socket failed");
    printf("[-] socket success!\n");

    /* binding */
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(port);
    printf("port: %d\n", port);
    if (bind(sockfd, (struct sockaddr*)&addr, sizeof(struct sockaddr_in)) < 0)
	print_and_exit("[x] bind failed");
    printf("[-] bind success!\n");

    /* listening */
    if (listen(sockfd, 1000) < 0)
	print_and_exit("[x] listen failed");
    printf("[-] start listening\n");

    /* accepting */
    in_addr_size = sizeof(struct sockaddr_in);
    connfd = accept(sockfd, (struct sockaddr*)&in_addr, &in_addr_size);
    if (connfd < 0) print_and_exit("[x] accept failed");
    printf("[-] accept!\n");

    while (1) {
	sleep(1);
    }
    /* receiving */
    while ((size = recv(connfd, buf, 128, 0)) > 0) {
	write(1, buf, size);
    }

    /* exiting */
    if (size == 0) {
	printf("[-] Exiting!\n");
    } else {
	print_and_exit("[x] error in recv");
    }
    close(connfd);
    close(sockfd);
}

void *do_nothing(void *a) {
    sleep(5);
}

int main(int argc, char *argv[]) {
    int i, j, NUM, NUM_S;
    pid_t pid;
    if (argc < 3) {
	printf("usage: tsendmsg [num_cps] [num_s]\n");
	exit(1);
    }
    NUM = strtol(argv[1], NULL, 0);
    NUM_S = strtol(argv[2], NULL, 0);
    str0 = malloc(TEST_BUF);
    str1 = malloc(SEND_BUF);
    for (j = 0; j < NUM_S; j++) {
	if (fork() == 0) {
	    pid = fork();
	    if (pid == 0) {
		sleep(1);
		for (i = 0; i < NUM; i++) {
		    if (fork() == 0) {
			do_client(j+8000);
			while (1) {
			    sleep(1);
			}
		    }
		}
	    } else {
		do_server(j+8000);
		/* will sleep forever */
	    }
	    while (1) {
		sleep(1);
	    }
	}
    }
    printf("done forking\n");
    while (1) {
	sleep(1);
    }
    return 0;
}
