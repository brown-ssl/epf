import os
from pprint import pprint

if __name__ == '__main__':
    d = {}
    for root, _, files in os.walk('log'):
        for name in files:
            filename = os.path.join(root, name)
            with open(filename) as f:
                s = f.read()
                d[s] = d.get(s, 0) + 1
    pprint(d)
                    
