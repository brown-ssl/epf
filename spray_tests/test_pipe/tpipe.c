#define _GNU_SOURCE
#include <stdio.h>
#include <fcntl.h>  
#include <unistd.h>
#include <limits.h>
#include <stdlib.h>

#define TEST_BUF 4096
#define MUL 32768

int test_pipe(int commfd, int j) {
    int pipe[MUL][2], err, i, k;
    char snd[TEST_BUF];
    char rcv[TEST_BUF];
    long value = 0;
    write(commfd, &value, sizeof(value));
    for (i = 0; i < TEST_BUF; i++) {
        snd[i] = 0xaa;
        rcv[i] = 0x00;
    }
    for (k = 0; k < MUL; k++){
	if (pipe2(pipe[k], 0)) {
	    printf("[x] no pipe\n");
            goto exit;
	};
	/* printf("[-] pipe created, pipe[0] = %d, pipe[1] = %d\n", pipe[k][0], pipe[k][1]); */
	if (write(pipe[k][1], snd, TEST_BUF) != TEST_BUF) {
	    printf("[x] write failed\n");
	    goto exit;
	};
	value = TEST_BUF;
	write(commfd, &value, sizeof(value));
	/* close(pipe[k][0]); */
	/* if (read(pipe[k][0], rcv, TEST_BUF) != TEST_BUF) { */
	/*     printf("[x] read failed\n"); */
	/*     exit(1); */
	/* }; */
	/* printf("[-] read finished\n"); */
    }
exit:
    value = -1;
    write(commfd, &value, sizeof(value));
    while (1) {
        usleep(1000000);
    }
    return 0;
}
