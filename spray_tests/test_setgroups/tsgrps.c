#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sched.h>

#define STACK_SIZE (1024 * 1024)

int g_commfd;

void err_exit(const char *msg)
{
    long v = -1;
    perror(msg);
    write(g_commfd, &v, sizeof(v));
    exit(0);
}

int do_stuff(void *arg)
{
    printf("haha\n");
}

void test_setgroups(int commfd)
{
    long mem = 0;
    char *stack, *stack_top;
    g_commfd = commfd;
    stack = malloc(STACK_SIZE);
    if (stack == NULL)
	err_exit("malloc");
    stack_top = stack + STACK_SIZE;
    write(g_commfd, &mem, sizeof(mem));
    if (clone(do_stuff, stack_top, CLONE_NEWUSER, NULL) == -1) {
	err_exit("clone");
    }
    while (1) {
	sleep(10);
    }
}
