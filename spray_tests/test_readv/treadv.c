#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/uio.h>

#define N 1024
#define L 10
void test_readv(int commfd, int ignore)
{
    int i;
    char buf[1024];
    long mem = 0;
    write(commfd, &mem, sizeof(mem));
    struct iovec iov[N];
    for (i = 0; i < N; i++) {
	iov[i].iov_base = (char *)0xdeadbeef;
	iov[i].iov_len = 0xfeedcafe;
    }
    mem = N * sizeof(struct iovec);
    write(commfd, &mem, sizeof(mem));
    if (readv(0, iov, N) < 0) {
	mem = -mem;
	write(commfd, &mem, sizeof(mem));
	perror("readv");
    }
    while (1) {
	sleep(10);
    }
}
