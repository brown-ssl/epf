#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <sys/wait.h>

#include <netinet/ip.h>

static int server(int port, int commfd) {
    int sockfd, c;
    long value = -1;
    char buf[128];
    struct sockaddr_in c_addr, s_addr = {
	.sin_family = AF_INET,
	.sin_port = htons(port),
	.sin_addr = htonl(INADDR_ANY),
    };
    printf("[s] server here\n");
    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
	perror("[s] error socket");
	write(commfd, &value, sizeof(value));
	exit(1);
    }
    if (bind(sockfd, (struct sockaddr*) &s_addr, sizeof(s_addr)) < 0) {
	perror("[s] error bind");
	write(commfd, &value, sizeof(value));
	exit(1);
    } else printf("[s] bind success\n");
    printf("[s] shutting down\n");
}

#define SIZE 4000
static int client(int port, int commfd) {
    int sockfd, timeout=0;
    long value, sum;
    char *buf = malloc(SIZE);
    memset(buf, 'x', SIZE);
    struct sockaddr_in s_addr = {
	.sin_family = AF_INET,
	.sin_port = htons(port),
	.sin_addr = htonl(INADDR_LOOPBACK),
    };
    value = 0;
    write(commfd, &value, sizeof(value));
    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) perror("[c] error socket");
    sum = 0;
    while ((value = sendto(sockfd, buf, SIZE, 0, (struct sockaddr *) &s_addr, sizeof(s_addr))) > 0) {
	sum += value;
	write(commfd, &value, sizeof(value));
	if (sum > 180000) break;
    }
    if (value < 0) {
	perror("sendto");
	value = -1;
	write(commfd, &value, sizeof(value));
    }
    close(sockfd);
}

void test_send_udp(int commfd, int j) {
    pid_t pid;
    int port, i;
    for (i = 0; i < 1000; i++) {
	printf("haha\n");
	port = 8000 + i + j * 1000;
	server(port, commfd);
	client(port, commfd);
    }
    while (1) {
	sleep(10);
    }
    exit(0);
}
