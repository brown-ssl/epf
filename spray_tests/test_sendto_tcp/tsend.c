#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <sys/wait.h>

#include <netinet/ip.h>

static int server(int port, int commfd) {
    int sockfd, connfd, c;
    long value = -1;
    char buf[128];
    struct sockaddr_in c_addr, s_addr = {
	.sin_family = AF_INET,
	.sin_port = htons(port),
	.sin_addr = htonl(INADDR_ANY),
    };
    printf("[s] server here\n");
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
	perror("[s] error socket");
	write(commfd, &value, sizeof(value));
	exit(1);
    }
    if (bind(sockfd, (struct sockaddr*) &s_addr, sizeof(s_addr)) < 0) {
	perror("[s] error bind");
	write(commfd, &value, sizeof(value));
	exit(1);
    }
    if (listen(sockfd, 10) < 0) {
	perror("[s] error listen");
	write(commfd, &value, sizeof(value));
	exit(1);
    }
    if ((connfd = accept(sockfd, NULL, NULL)) < 0) {
	perror("[s] error accept");
	write(commfd, &value, sizeof(value));
	exit(1);
    }
    else printf("[s] accept success!\n");
    while (1) {
	sleep(10);
    }
    while ((c = recv(connfd, &buf, 128, 0)) > 0) {
	write(1, buf, c);
    }
    printf("\n");
    close(connfd);
    close(sockfd);
    printf("[s] shutting down\n");
}

#define SIZE 4000
static int client(int port, int commfd) {
    int sockfd, timeout=0;
    long value;
    char *buf = malloc(SIZE);
    memset(buf, 'x', SIZE);
    struct sockaddr_in s_addr = {
	.sin_family = AF_INET,
	.sin_port = htons(port),
	.sin_addr = htonl(INADDR_LOOPBACK),
    };
    value = 0;
    write(commfd, &value, sizeof(value));
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) perror("[c] error socket");
    while (connect(sockfd, (struct sockaddr*) &s_addr, sizeof(s_addr)) < 0) {
	if (timeout > 3) {
	    perror("[c] cannot connect");
	    break;
	}
	printf("[c] trying %d\n", timeout);
	usleep(1000);
	timeout++;
    };
    while ((value = send(sockfd, buf, SIZE, 0)) > 0) {
	write(commfd, &value, sizeof(value));
    }
    value = -1;
    write(commfd, &value, sizeof(value));
    perror("[c] error send");
    close(sockfd);
}

void test_send(int commfd, int i) {
    pid_t pid;
    int port;
    pid = fork();
    port = 8000 + i;
    if (pid == 0) {
	//int fd = open("/dev/null", O_RDWR, 0);
	//dup2(fd, 0);
	//dup2(fd, 1);
	server(port, commfd);
    } else {
	client(port, commfd);
    }
    while (1) {
	sleep(10);
    }
}
