#include <stdio.h>         /* printf */
#include <stdlib.h>
#include <sys/prctl.h>     /* prctl */
#include <linux/seccomp.h> /* seccomp's constants */
#include <unistd.h>        /* dup2: just for test */
#include <linux/filter.h>

#define INST(CELL, OP, JT, JF, K) \
    do {\
    (CELL).code = (OP);\
    (CELL).jt = (JT);\
    (CELL).jf = (JF);\
    (CELL).k = (K);\
    } while (0)

long count = 0;

int seccomp() {
    int i, size = 512;
    struct sock_filter *code = (struct sock_filter *)malloc(size * sizeof(struct sock_filter));
    INST(code[0], 0x06, 0x0, 0x0, SECCOMP_RET_ALLOW);
    for (i = 1; i < size; i++) {
	INST(code[i], 0x16, 0x0, 0x0, SECCOMP_RET_ALLOW);
    };
    struct sock_fprog *bpf = (struct sock_fprog *)malloc(sizeof(struct sock_fprog));
    bpf->len = size;
    bpf->filter = code;
    count = size * sizeof(struct sock_filter);
    return prctl(PR_SET_SECCOMP, SECCOMP_MODE_FILTER, bpf);
}

void test_seccomp(int commfd, int j) {
    int i;
    if (prctl(PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0) < 0) {
	perror("permission set");
    };
    write(commfd, &count, sizeof(count));
    for (i = 0; i < 100; i++) {
	if (seccomp() < 0) {
	    perror("seccomp");
	    long tmp = -1;
	    write(commfd, &tmp, sizeof(tmp));
	    break;
	}
	write(commfd, &count, sizeof(count));
    }
    while(1) {
	sleep(10);
    }
}
