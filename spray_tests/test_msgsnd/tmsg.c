#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/msg.h>
#include <sys/ipc.h>

#define SIZE 8192
#define N 2
#define QNUM 100

struct mybuffer {
    long mtype;
    char text[SIZE+100];
};

void test_msg(int commfd, int ignore)
{
    struct mybuffer msg;
    int qid[QNUM], i, j;
    long mem = 0;
    write(commfd, &mem, sizeof(mem));
    msg.mtype = 1;
    for (j = 0; j < QNUM; j++) {
	qid[j] = msgget(IPC_PRIVATE, 0644);
	if (qid[j] < 0) {
	    perror("msgget");
	    mem = -1;
	    write(commfd, &mem, sizeof(mem));
	}
	for (i = 0; i < N; i++) {
	    if (msgsnd(qid[j], &msg, SIZE, 0) < 0) {
		perror("msgsnd");
		mem = -1;
		write(commfd, &mem, sizeof(mem));
	    } else {
		mem = SIZE;
		write(commfd, &mem, sizeof(mem));
	    }
	}
    }
    while (1) {
	sleep(10);
    }
}
