From ae6d0109549bdf6f7753efb1d204db19fa2dc81d Mon Sep 17 00:00:00 2001
From: Tenut <tenut@Niobium>
Date: Tue, 23 Mar 2021 12:07:35 -0400
Subject: [PATCH 1/4] Add BPF ISR

Mask constant field in BPF instructions with random value to avoid malicious control over kernel memory content
---
 include/linux/filter.h |  1 +
 init/Kconfig           | 15 +++++++++++++++
 kernel/bpf/core.c      | 39 ++++++++++++++++++++++++++++++++-------
 3 files changed, 48 insertions(+), 7 deletions(-)

diff --git a/include/linux/filter.h b/include/linux/filter.h
index 1b62397bd..13ad5df9a 100644
--- a/include/linux/filter.h
+++ b/include/linux/filter.h
@@ -542,6 +542,7 @@ struct bpf_prog {
 	u8			tag[BPF_TAG_SIZE];
 	struct bpf_prog_aux	*aux;		/* Auxiliary fields */
 	struct sock_fprog_kern	*orig_prog;	/* Original BPF program */
+	__s32			mask;
 	unsigned int		(*bpf_func)(const void *ctx,
 					    const struct bpf_insn *insn);
 	/* Instructions for interpreter */
diff --git a/init/Kconfig b/init/Kconfig
index 0872a5a2e..c16f7529f 100644
--- a/init/Kconfig
+++ b/init/Kconfig
@@ -1722,6 +1722,21 @@ config BPF_JIT_DEFAULT_ON
 	def_bool ARCH_WANT_DEFAULT_BPF_JIT || BPF_JIT_ALWAYS_ON
 	depends on HAVE_EBPF_JIT && BPF_JIT
 
+config BPF_HARDENING
+	bool "Enable BPF interpreter hardening"
+	select BPF
+	depends on X86_64 && !RANDOMIZE_MEMORY && !BPF_JIT_ALWAYS_ON
+	default n
+	help
+	  Enhance bpf interpreter's security
+
+config BPF_ISR
+	bool "Enable BPF instruction set randomization"
+	depends on BPF_HARDENING
+	default n
+	help
+	  Randomize in-memory representation of eBPF programs
+
 source "kernel/bpf/preload/Kconfig"
 
 config USERFAULTFD
diff --git a/kernel/bpf/core.c b/kernel/bpf/core.c
index 55454d227..167c17d59 100644
--- a/kernel/bpf/core.c
+++ b/kernel/bpf/core.c
@@ -54,7 +54,12 @@
 #define AX	regs[BPF_REG_AX]
 #define ARG1	regs[BPF_REG_ARG1]
 #define CTX	regs[BPF_REG_CTX]
+
+#ifdef CONFIG_BPF_ISR
+#define IMM (insn->imm^mask)
+#else
 #define IMM	insn->imm
+#endif
 
 /* No hurry in this branch
  *
@@ -1389,6 +1394,13 @@ static u64 ___bpf_prog_run(u64 *regs, const struct bpf_insn *insn, u64 *stack)
 #undef BPF_INSN_2_LBL
 	u32 tail_call_cnt = 0;
 
+#ifdef CONFIG_BPF_ISR
+	__s32 mask;
+	const struct bpf_prog *fp;
+	fp = container_of(insn, struct bpf_prog, insnsi[0]);
+	mask = fp->mask;
+#endif
+
 #define CONT	 ({ insn++; goto select_insn; })
 #define CONT_JMP ({ insn++; goto select_insn; })
 
@@ -1438,8 +1450,9 @@ static u64 ___bpf_prog_run(u64 *regs, const struct bpf_insn *insn, u64 *stack)
 		DST = IMM;
 		CONT;
 	LD_IMM_DW:
-		DST = (u64) (u32) insn[0].imm | ((u64) (u32) insn[1].imm) << 32;
+		DST = (u64) (u32) IMM;
 		insn++;
+		DST |= ((u64) (u32) IMM) << 32;
 		CONT;
 	ALU_ARSH_X:
 		DST = (u64) (u32) (((s32) DST) >> SRC);
@@ -1518,15 +1531,15 @@ static u64 ___bpf_prog_run(u64 *regs, const struct bpf_insn *insn, u64 *stack)
 		 * preserves BPF_R6-BPF_R9, and stores return value
 		 * into BPF_R0.
 		 */
-		BPF_R0 = (__bpf_call_base + insn->imm)(BPF_R1, BPF_R2, BPF_R3,
-						       BPF_R4, BPF_R5);
+		BPF_R0 = (__bpf_call_base + IMM)(BPF_R1, BPF_R2, BPF_R3,
+						 BPF_R4, BPF_R5);
 		CONT;
 
 	JMP_CALL_ARGS:
-		BPF_R0 = (__bpf_call_base_args + insn->imm)(BPF_R1, BPF_R2,
-							    BPF_R3, BPF_R4,
-							    BPF_R5,
-							    insn + insn->off + 1);
+		BPF_R0 = (__bpf_call_base_args + IMM)(BPF_R1, BPF_R2,
+						      BPF_R3, BPF_R4,
+						      BPF_R5,
+						      insn + insn->off + 1);
 		CONT;
 
 	JMP_TAIL_CALL: {
@@ -1785,6 +1798,17 @@ static void bpf_prog_select_func(struct bpf_prog *fp)
 #endif
 }
 
+static void bpf_mask_prog(struct bpf_prog *fp)
+{
+#ifdef CONFIG_BPF_ISR
+	int i;
+	get_random_bytes(&fp->mask, sizeof(fp->mask));
+	for (i = 0; i < fp->len; i++) {
+		*((__s32*)&fp->insnsi[i]+1) ^= fp->mask;
+	}
+#endif /* CONFIG_BPF_ISR */
+}
+
 /**
  *	bpf_prog_select_runtime - select exec runtime for BPF program
  *	@fp: bpf_prog populated with internal BPF program
@@ -1831,6 +1855,7 @@ struct bpf_prog *bpf_prog_select_runtime(struct bpf_prog *fp, int *err)
 	}
 
 finalize:
+	bpf_mask_prog(fp);
 	bpf_prog_lock_ro(fp);
 
 	/* The tail call compatibility check can only be done at
-- 
2.20.1

