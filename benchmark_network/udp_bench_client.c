#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <pthread.h>
#include <linux/filter.h>
#include <sys/time.h>
#include <sched.h>

#define BUFSIZE 64
#define N 5

/*
 * error - wrapper for perror
 */
void error(char *msg) {
    perror(msg);
    exit(1);
}

void pin_to_cpu(int cpu) {
    cpu_set_t set;
    CPU_ZERO(&set);
    CPU_SET(cpu, &set);
    if (sched_setaffinity(0, sizeof(set), &set) == -1)
	 error("sched_setaffinity");
}

int stopped = 0;
struct child {
    long count;
    int started;
    int run;
    int index;
};
struct child children[N];
int client_cpus[] = {0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14};
char *hostname;
int portno;
pthread_t client_thread[N];

long total = 0;

void *send_until_stopped(void *arg) {
    struct sockaddr_in server_addr;
    struct hostent *server;
    struct child *me = arg;
    int sockfd, i, ret;
    char buf[BUFSIZE];

    /* pin_to_cpu(client_cpus[me->index]); */
    
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");


    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    /* memcpy(&server_addr.sin_addr.s_addr, server->h_addr, server->h_length); */
    inet_pton(AF_INET, hostname, &server_addr.sin_addr.s_addr);
    server_addr.sin_port = htons(portno);

    memset(buf, 'c', BUFSIZE);

    me->started = 1;
    while (!me->run) {usleep(100);}
    printf("huh\n");
    i = 0;
    while (!stopped) {
	ret = sendto(sockfd, buf, BUFSIZE, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
	me->count ++;
	if (i == 0x100000) {
		i = 0;
	} else {
		i ++;
	}
    }
    return NULL;
}

int main(int argc, char **argv) {
    int sockfd; /* socket */
    int clientlen; /* byte size of client's address */
    struct sockaddr_in serveraddr; /* server's addr */
    struct sockaddr_in clientaddr; /* client addr */
    struct hostent *hostp; /* client host info */
    struct sock_fprog *bpf_prog;
    char buf[BUFSIZE]; /* message buf */
    char *hostaddrp; /* dotted decimal host addr string */
    int optval; /* flag value for setsockopt */
    int n; /* message byte size */
    int run_server = 0, run_client = 0, server_side_bpf = 0;
    int i;
    struct timeval start, end;
    long last = 0;

    if (argc != 3) {
       fprintf(stderr,"usage: %s <hostname> <port>\n", argv[0]);
       exit(0);
    }
    hostname = argv[1];
    portno = atoi(argv[2]);

    for (i = 0; i < N; i++) {
	children[i].started = 0;
	children[i].run = 0;
	children[i].index = i;
	children[i].count = 0;
	pthread_create(&client_thread[i], NULL, send_until_stopped, &children[i]);
    }
    printf("created %d things\n", N);
    for (i = 0; i < N; i++) {
	while (!children[i].started) {usleep(10);};
	printf("%d has started\n", i);
    }
    for (i = 0; i < N; i++) {
	children[i].run = 1;
	printf("running %d\n", i);
    }
    while (1) {
        long tmp = 0;
        sleep(1);
	gettimeofday(&end, NULL);
	timersub(&end, &start, &start);
	for (i = 0; i < N; i++) tmp += children[i].count;
	printf("%.6f\n", (tmp - last) / (double)(start.tv_sec + (double)(start.tv_usec)/1000000));
	last = tmp;
	memcpy(&start, &end, sizeof(start));
    }
    return 0;
}
