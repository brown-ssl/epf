#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <pthread.h>
#include <linux/filter.h>
#include <sys/time.h>
#include <sched.h>

#define BUFSIZE 1024
#define N 0

/*
 * error - wrapper for perror
 */
void error(char *msg) {
    perror(msg);
    exit(1);
}

void pin_to_cpu(int cpu) {
    cpu_set_t set;
    CPU_ZERO(&set);
    CPU_SET(cpu, &set);
    if (sched_setaffinity(0, sizeof(set), &set) == -1)
	 error("sched_setaffinity");
}

char *hostname;
int portno;

struct sock_fprog *create_bpf_rule(int num) {
    int i, size;
    struct sock_fprog *bpf_prog;
    struct sock_filter *insns;
    size = (num*3 + 1) * sizeof(struct sock_filter);
    insns = (struct sock_filter*)malloc(size);
    memset(insns, 0, size);
    for (i = 0; i < num; i++) {
	insns[3*i].code = BPF_LD | BPF_H | BPF_ABS;
	insns[3*i].k = 12;
	insns[3*i+1].code = BPF_JMP | BPF_JEQ | BPF_K;
	insns[3*i+1].jf = 1;
	insns[3*i+1].k = 0x1000;
	insns[3*i+2].code = BPF_RET | BPF_K;
	insns[3*i+2].k = 0x1000;
    }
    insns[3*i].code = BPF_RET | BPF_K;
    insns[3*i].k = 0x1000;
    bpf_prog = (struct sock_fprog*)malloc(sizeof(*bpf_prog));
    bpf_prog->len = 3*num+1;
    bpf_prog->filter = insns;
    return bpf_prog;
}

int main(int argc, char **argv) {
    int sockfd; /* socket */
    int clientlen; /* byte size of client's address */
    struct sockaddr_in serveraddr; /* server's addr */
    struct sockaddr_in clientaddr; /* client addr */
    struct hostent *hostp; /* client host info */
    struct sock_fprog *bpf_prog;
    char buf[BUFSIZE]; /* message buf */
    char *hostaddrp; /* dotted decimal host addr string */
    int optval; /* flag value for setsockopt */
    long total = 0;
    int n, repeat; /* message byte size */
    int run_server = 0, run_client = 0, server_side_bpf = 0;
    long i;
    struct timeval start, end;

    if (argc > 1 && (strcmp(argv[1], "-f") == 0)) {
	server_side_bpf = 1;
	argc--;
	argv++;
    }

    if (argc != 3) {
       fprintf(stderr,"usage: %s <hostname> <port>\n", argv[0]);
       exit(0);
    }
    hostname = argv[1];
    portno = atoi(argv[2]);

    /* pin_to_cpu(11); */
    
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
	error("ERROR opening socket");

    /* setsockopt: Handy debugging trick that lets
     * us rerun the server immediately after we kill it;
     * otherwise we have to wait about 20 secs.
     * Eliminates "ERROR on binding: Address already in use" error.
     */
    optval = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR,
	       (const void *)&optval , sizeof(int));

    bpf_prog = create_bpf_rule(100);
    if (server_side_bpf)
	if (setsockopt(sockfd, SOL_SOCKET, SO_ATTACH_FILTER, bpf_prog, sizeof(*bpf_prog))) {
	    perror("setsockopt ATTACH_FILTER");
	    return 1;
	}

    memset(&serveraddr, 0, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
    serveraddr.sin_port = htons((unsigned short)portno);

    if (bind(sockfd, (struct sockaddr *) &serveraddr,
	     sizeof(serveraddr)) < 0)
	error("ERROR on binding");

    clientlen = sizeof(clientaddr);
    if (recvfrom(sockfd, buf, BUFSIZE, 0, (struct sockaddr *) &clientaddr, &clientlen) < 0)
	error("ERROR in recvfrom");
    for (repeat = 0; repeat < 10; repeat++) {
	gettimeofday(&start, NULL);
	i = 0;
	while (i < 0x100000) {
	    n = recvfrom(sockfd, buf, BUFSIZE, 0,
			 (struct sockaddr *) &clientaddr, &clientlen);
	    if (n < 0)
		error("ERROR in recvfrom");

	    total += n;
	    i += 1;
	};
	gettimeofday(&end, NULL);
	printf("%.6f\n", ((double) i) * 1000000 / ((end.tv_sec * 1000000 + end.tv_usec)
						   - (start.tv_sec * 1000000 + start.tv_usec)));
    }
    return 0;
}
